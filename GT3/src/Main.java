import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ArrayList<Keyword> keywords = new ArrayList<>();
	BeatGoogle beatGoogle = new BeatGoogle(keywords);

	public Main() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		
//		String keyword = request.getParameter("keyword"); 
//		byte[] bytes = keyword.getBytes(StandardCharsets.ISO_8859_1);
//		keyword = new String(bytes, StandardCharsets.UTF_8);
		
		
		String keyword = request.getParameter("keyword");
		OutputStream out = response.getOutputStream();
		PrintWriter outWriter = new PrintWriter(out);
		StringBuffer returnData = new StringBuffer();

		keywords.add(new Keyword(keyword, 1));
		keywords.add(new Keyword("torrent", 2));
		keywords.add(new Keyword("download", 3));

		ArrayList<Website> resultWebs = beatGoogle.query(keyword);
		for (Website w : resultWebs) {

			returnData.append("<a href=\"" + w.urlStr
					+ "\"       target=\"__blank\">Download Score: "
					+ w.globalScore + "</a><br><br>");
		}

		outWriter.println(returnData);
		outWriter.flush();
		outWriter.close();

	}
}
