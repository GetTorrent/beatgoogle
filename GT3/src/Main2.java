//package edu.nccu.soslab.beatgoogle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.net.URLEncoder;

public class Main2 {
	public static void main(String[] args) throws IOException {
		Scanner input = new Scanner(System.in);
		String keyword = input.next();

		ArrayList<Keyword> keywords = new ArrayList<>();
		keywords.add(new Keyword(keyword, 1));
		keywords.add(new Keyword("torrent", 2));
		keywords.add(new Keyword("download", 3));

		BeatGoogle beatGoogle = new BeatGoogle(keywords);
		keyword = URLEncoder.encode(keyword, "utf-8");
		ArrayList<Website> resultWebs = beatGoogle.query(keyword);
		for (Website w : resultWebs) {
			System.out.println(w.title+" <a href=\"" + w.urlStr
					+ "\"       target=\"__blank\">Download Score: "
					+ w.globalScore + "</a><br><br>");
		}
		input.close();
	}
}
